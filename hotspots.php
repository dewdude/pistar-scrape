<html>
<head>
<meta content="text/html; charset=ISO-8859-1"
http-equiv="content-type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>URCALL Hotspot Info</title>
</head>
<body>
<div style="margin-right: auto; font-family: Verdana; text-align: left;"><big>
</big><small><span style="font-weight: bold;">URCALL Pi-Star Hotspot
Network</span><br>
<br style="font-weight: bold; font-style: italic;">
<span style="font-weight: bold; font-style: italic;">Current
Connections:</span><br>
</small><big>
</big>
<table
style="border: 1px solid black; text-align: left; border-collapse: collapse;"
cellpadding="2" cellspacing="0">
<tbody>
<?php
$output = shell_exec('/path/to/hotspots.sh');
echo "$output";
?>
</tbody>
</table>
<big>
</big><small><br style="font-style: italic; font-weight: bold;">
<span style="font-style: italic; font-weight: bold;">Listed connections
do not mean I have a radio on or am at a radio.<br>This is especially true
for Brandmeister.</span><br
style="font-weight: bold; font-style: italic;">
<span style="font-style: italic; font-weight: bold;"> For informational
purposes only.
</span><br>
<br>
73 de URCALL
<br>
<?php
echo date("Y-m-d") . " @ " . date("H:i:s") . "z";
?>
</small>
</div>
</body>
</html>
