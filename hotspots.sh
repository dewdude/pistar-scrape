#!/bin/bash
#.013 alpha
# de NQ4T (nq4tango@gmail.com)


# grab copy of rendered bm_links.php from hotspot, convert HTML to newline, filter by "TG", remove timeslot info, convert to one line
# check contents to see if we need to display 'no groups' message
# if you are not using DMR, then comment these two lines out
# to comment out, just add a pound-sign to the start of the line, like these comments.

dmr=$(curl -s http://IP.OF.PISTAR/mmdvmhost/bm_links.php| sed 's/<[^>]\+>/\n/g' | grep '^TG' |  sed 's/TG/#/g'| sed 's/(.)//g' | sed ':a;N;$!ba;s/\n/ - /g')
[ -z "$dmr" ] && dmr="No Talkgroups Found"

# do the same for dstar with repeaterinfo.php but just egrep for reflector prefix, strip HTML, keep the first 8 characters
# check to see if we need to display not linked
# if you are not using D-Star, comment these next two lines out.

dstar=$(curl -s http://IP.OF.PISTAR/mmdvmhost/repeaterinfo.php | egrep "REF|XRF|DCS|XLX" | sed 's/<[^>]\+>//g' | cut -b 1-8)
[ -z "$dstar" ] && dstar="Not Linked"

# HTML for table cells on hotspot.php page. These are all one line even if they look like two in your editor.

# This is for a 2x2 table using DMR and D-Star

printf "<tr><td style=\"vertical-align: top; font-family: Verdana; font-weight: bold; text-align: right;\" width=\"20%%\"><small>BM TG:</small></td>\n<td style=\"vertical-align: top;\"><small>$dmr</small><br></td></tr>\n<tr><td style=\"vertical-align: top; font-family: Verdana; font-weight: bold; text-align: right;\" width=\"20%%\"><small>D-Star:</small></td>\n<td style=\"vertical-align: top;\"><small>$dstar</small><br></td></tr>\n"
# This is for a DMR only display.
# printf "<tr><td style=\"vertical-align: top; font-family: Verdana; font-weight: bold; text-align: right;\" width=\"20%%\"><small>BM TG:</small></td>\n<td style=\"vertical-align: top;\"><small style=\"font-style: italic;\">$dmr</small><br></td></tr>\n"

# This is for a D-STar Only Display
# printf "<tr><td style=\"vertical-align: top; font-family: Verdana; font-weight: bold; text-align: right;\"width=\"20%%\"><small>D-Star:</small></td>\n<td style=\"vertical-align: top;\"><small style=\"font-style: italic;\">$dstar</small><br></td></tr>\n"


